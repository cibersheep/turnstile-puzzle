/*
 * Copyright (C) 2020  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Turnstile Puzzle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

Item {
    signal colorsUpdated

    property int circleSize: units.gu(40)
    //How many color pieces to shift every movement (per snap rotation)
    property int piecesMovedByRevolution: 2
    //Degrees to round to make circle to snap to
    property int angleToSnap: 60
    property bool showSelection: false

    property var circleColor: {
        "center": "",
        //Logic colors (angle ignored) 012 top, 345 right, 678 bottom, 9 10 11 left
        "colorOrdered": []
    }

    property var visualCircleColor: {
        //visual colors (with angle) 012 top, 345 right, 678 bottom, 9 10 11 left
        "colorOrdered": []
    }

    onCircleColorChanged: {
        var position = thePiece.rotation / angleToSnap
        var current = 0

        for (var i=0; i < visualCircleColor.colorOrdered.length; i++) {
            current = i + (position * piecesMovedByRevolution)

            if (current > 11) {
                current = current - 12
            } else if (current < 0) {
                current = current + 12
            }

            visualCircleColor.colorOrdered[i] = circleColor.colorOrdered[current]
        }

        visualCircleColorChanged()
    }

    width: circleSize
    height: width

    function rotateColors(from, to) {
        if (from == to) {
            return
        }

        if (from < 0) {
            from = 360 + from
        }

        if (to < 0) {
            to = 360 + to
        }

        var revolution = to / angleToSnap - from / angleToSnap

        if (revolution < 0) {
            for (var i=0; i < Math.abs(revolution); i++) {
                for (var j=0; j < piecesMovedByRevolution; j++) {
                    var element = circleColor.colorOrdered.shift()
                    circleColor.colorOrdered.push(element)
                }
            }
        } else {
            for (var i=0; i < Math.abs(revolution); i++) {
                for (var j=0; j < piecesMovedByRevolution; j++) {
                    var element = circleColor.colorOrdered.pop()
                    circleColor.colorOrdered.unshift(element)
                }
            }
        }

        circleColorChanged()
        colorsUpdated()
    }

    Item {
        id: thePiece
        width: circleSize
        height: width

        property real angle: 0
        property int previousRotation: 0

        Repeater {
            model: ["topLeft","topCenter","topRight","rightTop","rightCenter","rightBottom","bottomRight","bottomCenter","bottomLeft","leftBottom","leftCenter","leftTop"]

            Image {
                sourceSize.width: parent.width
                sourceSize.height: parent.height
                source: "../../assets/pieces/%1-%2.svg".arg(modelData).arg(visualCircleColor.colorOrdered[index])
            }
        }

        Image {
            sourceSize.width: parent.width
            sourceSize.height: parent.height
            source: "../../assets/pieces/centerCenter-%1.svg".arg(circleColor.center)
        }

        Image {
            sourceSize.width: parent.width
            sourceSize.height: parent.height
            source: "../../assets/pieces/selected.svg"
            visible: showSelection
        }

        rotation: drag.distance
    }

    //MouseArea needs to be outside of the element that rotates
    MouseArea {
        id: drag
        width: circleSize
        height: width

        property int distance: 0
        property int pressedX: 0
        property int pressedY: 0
        property bool dragging: false

        onPressed: {
            thePiece.previousRotation = thePiece.rotation
            parent.showSelection = true
            dragging = true
            pressedX = mouseX
            pressedY = mouseY
        }

        onReleased: {
            //Snap the circle to a position 0, 60, 120, 180, 240, 300 < 361º
            distance = (Math.round(distance / angleToSnap) *  angleToSnap)
            //Keep track of the current angle for the next interaction
            thePiece.angle = distance
            pressedX = 0
            pressedY = 0
            dragging = false
            parent.rotateColors(thePiece.previousRotation, thePiece.rotation)
        }

        onPositionChanged: {
            //Previous angle + atan2 from the center of the circle to the updated mouse position - atan2 from the center of the piece to the first tap
            distance = (thePiece.angle + (Math.atan2(mouseY - parent.height / 2, mouseX - parent.height / 2) - Math.atan2(drag.pressedY - parent.width / 2, drag.pressedX - parent.width / 2)) * 180 / Math.PI) % 360;
        }
    }
}
