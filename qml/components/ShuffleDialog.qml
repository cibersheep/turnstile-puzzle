import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: shuffleDialog
    title: i18n.tr("Shuffle Puzzle")
    text: i18n.tr("Are you sure you want to shuffle the puzzle. This can't be undone")

    Button {
        id: mainAction
        text: i18n.tr("Shuffle")
        color: theme.palette.normal.negative

        onClicked: {
            mainPage.shuffle()
            PopupUtils.close(shuffleDialog)
        }
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: PopupUtils.close(shuffleDialog)
    }
}

